package com.cc.groupg.dao;

public class DAOException extends Exception {
    public DAOException(String message) {
        super(message);
    }
}
