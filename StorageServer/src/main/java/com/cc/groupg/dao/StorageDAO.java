package com.cc.groupg.dao;


import com.cc.groupg.node.BucketInformation;
import com.cc.groupg.node.EditResult;
import com.cc.groupg.node.RangeQueryResult;
import com.cc.groupg.node.SearchResult;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;

public class StorageDAO {

    private ArrayList<Item> cache;
    private String filepath;
    private Gson gson;

    public StorageDAO(String filepath) {
        this.cache = new ArrayList<>();
        String directoryName = "data";
        File file = new File(directoryName);
        if(!file.exists())
            file.mkdirs();
        this.filepath = directoryName + "/"+filepath;
        this.gson = new Gson();
    }

    public EditResult deleteItem(String key) {
        Item lineInCache = searchInCache(key).getResult();
        if (lineInCache!= null) {
            this.cache.remove(lineInCache);
        }
        try {
            File file = new File(filepath);
            File fileTemp = new File(filepath+"_temp_");
            if (!file.exists())
                file.createNewFile();
            FileReader fileReader = new FileReader(file);
            FileWriter fileWriter = new FileWriter(fileTemp);
            BufferedReader reader = new BufferedReader(fileReader);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                if (currentLine.length() == 0)
                    break;
                Item object = gson.fromJson(currentLine, Item.class);
                if (object.getKey().equals(key)) {
                    continue;
                }
                writer.write(gson.toJson(object));
                writer.newLine();
            }
            writer.close();
            reader.close();
            fileReader.close();
            fileWriter.close();
            file.delete();
            fileTemp.renameTo(file);
        } catch (Exception e) {
            e.printStackTrace();
            return new EditResult(false);
        }

        return new EditResult(true, new BucketInformation(filepath));
    }

    public EditResult addItem(Item item){
        Item foundItem = search(item.getKey()).getResult();
        if (foundItem == null) {
            try {
                File file = new File(filepath);
                if (!file.exists())
                    file.createNewFile();

                FileWriter fileWriter = new FileWriter(file, true);
                BufferedWriter writer = new BufferedWriter(fileWriter);
                writer.write(gson.toJson(item));
                writer.newLine();
                writer.close();
                fileWriter.close();
                if (this.cache.size()> 500)
                    this.cache.remove(0);
                this.cache.add(item);
                return new EditResult(true, new BucketInformation(filepath));
            } catch (Exception e) {
                e.printStackTrace();
                return new EditResult(false, new BucketInformation(filepath));
            }
        } else {
            return new EditResult(false, new BucketInformation(filepath));
        }
    }

    public SearchResult searchInCache(String key) {
        for (Item item : cache)
            if (item.getKey().equals(key))
                return new SearchResult(item, new BucketInformation(null));
        return new SearchResult(null, new BucketInformation(null));
    }

    public SearchResult search(String key) {
        try {
            File file = new File(filepath);
            if (!file.exists())
                file.createNewFile();

            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                if (currentLine.length() == 0)
                    break;
                Item object = gson.fromJson(currentLine, Item.class);
                if (object.getKey().equals(key)) {
                    fileReader.close();
                    reader.close();
                    return new SearchResult(object, new BucketInformation(filepath));
                }
            }
            fileReader.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new SearchResult(null, new BucketInformation(filepath));
    }

    public RangeQueryResult getElementsFromRange(String startKey, String endKey) {
        ArrayList<Item> items = new ArrayList<>();
        try {
            File file = new File(filepath);
            if (!file.exists())
                file.createNewFile();
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);
            String currentLine;
            while ((currentLine = reader.readLine()) != null) {
                if (currentLine.length() == 0)
                    break;
                Item object = gson.fromJson(currentLine, Item.class);
                if (object.getKey().compareTo(startKey) >= 0 && object.getKey().compareTo(endKey) <= 0) {
                    items.add(object);
                }
            }
            fileReader.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new RangeQueryResult(items, new BucketInformation(filepath));
    }
}