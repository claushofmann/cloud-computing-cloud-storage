package com.cc.groupg.node;

import com.cc.groupg.dao.Item;
import com.cc.groupg.dao.StorageDAO;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class DeleteTask implements Callable<EditResult> {
    StorageDAO dao;
    String key;
    public DeleteTask(StorageDAO dao,String key) {
        super();
        this.dao = dao;
        this.key = key;
    }

    @Override
    public EditResult call() {
        return dao.deleteItem(this.key);
    }

}
