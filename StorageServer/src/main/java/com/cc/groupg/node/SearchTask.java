package com.cc.groupg.node;

import com.cc.groupg.dao.Item;
import com.cc.groupg.dao.StorageDAO;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class SearchTask implements Callable<SearchResult> {
    StorageDAO dao;
    String key;
    public SearchTask(StorageDAO dao,String key) {
        super();
        this.dao = dao;
        this.key = key;
    }

    @Override
    public SearchResult call() {
        SearchResult result = dao.searchInCache(this.key);
        if(result.getResult() == null)
            result = dao.search(this.key);
        return result;
    }

}
