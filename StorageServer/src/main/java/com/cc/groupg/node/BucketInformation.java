package com.cc.groupg.node;

import java.util.ArrayList;
import java.util.List;

public class BucketInformation {

    private List<String> filesUsed;

    private List<Integer> nodeIdsUsed;

    public BucketInformation(){
        filesUsed = new ArrayList<>();
        nodeIdsUsed = new ArrayList<>();
    }

    public BucketInformation(List<String> filesUsed, List<Integer> nodeIdsUsed) {
        this.filesUsed = new ArrayList<>(filesUsed);
        this.nodeIdsUsed = new ArrayList<>(nodeIdsUsed);
    }

    public BucketInformation(String filename) {
        filesUsed = new ArrayList<>();
        filesUsed.add(filename);
        nodeIdsUsed = new ArrayList<>();
    }

    public List<String> getFilesUsed() {
        return filesUsed;
    }

    public List<Integer> getNodeIdsUsed() {
        return nodeIdsUsed;
    }

    public static BucketInformation withNodeId(int nodeId) {
        BucketInformation bucketInformation = new BucketInformation();
        bucketInformation.nodeIdsUsed.add(nodeId);
        return  bucketInformation;
    }

    public void merge(BucketInformation other) {
        filesUsed.addAll(other.filesUsed);
        nodeIdsUsed.addAll(other.nodeIdsUsed);
    }

    public void addNodeId(int nodeId) {
        nodeIdsUsed.add(nodeId);
    }
}
