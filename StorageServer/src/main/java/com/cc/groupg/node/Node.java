package com.cc.groupg.node;

import com.cc.groupg.dao.Item;
import com.cc.groupg.dao.StorageDAO;
import com.cc.groupg.hash.HashFunction;
import com.cc.groupg.rest.messages.SearchResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class Node {

    private String startHash;
    StorageDAO dao;
    StorageDAO previousReserveDAO;
    HashFunction hashFunction;
    int status;
    private ExecutorService storageExecutorService = Executors.newFixedThreadPool(1);
    private Node nextNode;
    private Node nextNodeReserve;
    private int nodeId;
    private Logger LOGGER = LoggerFactory.getLogger(Node.class);
    public Node(String startHash, String bucketName, HashFunction hashFunction, int nodeId) {
        this.startHash = startHash;
        this.dao = new StorageDAO(bucketName);
        this.previousReserveDAO = new StorageDAO(bucketName+"_previous_reserve");
        this.hashFunction = hashFunction;
        this.nodeId = nodeId;
        this.status = 1;
    }

    private EditResult handleEditResult(Future<EditResult> resultFuture) {
        try {
            EditResult result = resultFuture.get();
            result.getBucketInformation().addNodeId(nodeId);
            return result;
        } catch (Exception e) {
            LOGGER.error("Exception occured", e);
            return new EditResult(false, BucketInformation.withNodeId(nodeId));
        }
    }

    public EditResult insert(Item item,boolean inReserve) {
        InsertTask task;
        if(!inReserve) {
            this.nextNode.insert(item,true);
            task = new InsertTask(this.dao, item);
        } else
            task = new InsertTask(this.previousReserveDAO,item);
        Future<EditResult> resultFuture = storageExecutorService.submit(task);
        return handleEditResult(resultFuture);
    }
    public EditResult delete(String key, boolean inReserve) {
        DeleteTask task;
        if(!inReserve) {
            this.nextNode.delete(key,true);
            task = new DeleteTask(this.dao, key);
        } else
            task = new DeleteTask(this.previousReserveDAO, key);
        Future<EditResult> resultFuture = storageExecutorService.submit(task);
        return handleEditResult(resultFuture);
    }
    public SearchResult search(String key, boolean inReserve) {
        SearchTask task;
        if(!inReserve) {
            task = new SearchTask(this.dao, key);
        } else
            task = new SearchTask(this.previousReserveDAO, key);
        Future<SearchResult> resultFuture = storageExecutorService.submit(task);
        try {
            SearchResult result = resultFuture.get();
            result.getBucketInformation().addNodeId(nodeId);
            return result;
        } catch (Exception e) {
            LOGGER.error("Exception occured", e);
            return new SearchResult(null, BucketInformation.withNodeId(nodeId));
        }
    }
    public RangeQueryResult searchInRange(String startKey,String endKey,boolean inReserve) {
        RangeQueryResult results = new RangeQueryResult();
        RangeQueryTask task;
        if (!inReserve) {
            task = new RangeQueryTask(this.dao, startKey, endKey);
        } else {
            task = new RangeQueryTask(this.previousReserveDAO, startKey,endKey);
            if(this.getStartHash().compareTo(this.hashFunction.getHash(endKey))>=0) {
                results.merge(this.searchInRange(startKey, endKey,false));
            }
        }
        Future<RangeQueryResult> localResultFuture;
        localResultFuture = storageExecutorService.submit(task);
        if(this.nextNode != null &&
                hashFunction.getPositionByHash(this.nextNode.startHash)<=hashFunction.getPositionByHash(this.hashFunction.getHash(endKey))
                && hashFunction.getPositionByHash(this.startHash)!= hashFunction.getAmountOfNodes()-1
        ) {
            if(this.nextNode.getStatus() == 1) {
                results.merge(this.nextNode.searchInRange(startKey, endKey,false));
            } else {
                if(this.nextNode.getStatus() == 1)
                    results.merge(this.nextNodeReserve.searchInRange(startKey, endKey,true));
            }
        }
        try {
                RangeQueryResult localResult = new RangeQueryResult();
                if(this.getStatus() == 1)
                    localResult = localResultFuture.get();
                localResult.getBucketInformation().addNodeId(nodeId);
                results.merge(localResult);
        } catch (Exception e) {
            LOGGER.error("Exception occured", e);
        }
        return results;
    }

    public String getStartHash() {
        return startHash;
    }

    public void setNextNode(Node nextNode) {
        this.nextNode = nextNode;
        if(nextNode.nextNode != null)
            this.nextNodeReserve = nextNode.nextNode;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) { this.status = status;}
}
