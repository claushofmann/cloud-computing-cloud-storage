package com.cc.groupg.node;

import com.cc.groupg.dao.Item;
import com.cc.groupg.dao.StorageDAO;

import java.util.concurrent.Callable;

public class InsertTask implements Callable<EditResult> {
    StorageDAO dao;
    Item item;

    public InsertTask(StorageDAO dao,Item item) {
        super();
        this.dao = dao;
        this.item = item;
    }

    @Override
    public EditResult call() {
        return dao.addItem(this.item);
    }


}
