package com.cc.groupg.node;

public class EditResult {

    private Boolean success;

    private BucketInformation bucketInformation;

    public EditResult(Boolean success, BucketInformation bucketInformation) {
        this.success = success;
        this.bucketInformation = bucketInformation;
    }

    public EditResult(Boolean success) {
        this.success = success;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public BucketInformation getBucketInformation() {
        return bucketInformation;
    }

    public void setBucketInformation(BucketInformation bucketInformation) {
        this.bucketInformation = bucketInformation;
    }
}
