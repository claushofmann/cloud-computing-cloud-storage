package com.cc.groupg.node;

import com.cc.groupg.dao.Item;
import com.cc.groupg.dao.StorageDAO;

import java.util.ArrayList;
import java.util.concurrent.Callable;

public class RangeQueryTask implements Callable<RangeQueryResult> {
    StorageDAO dao;
    String startKey;
    String endKey;
    public RangeQueryTask(StorageDAO dao,String startKey, String endKey) {
        super();
        this.dao = dao;
        this.startKey = startKey;
        this.endKey = endKey;
    }

    @Override
    public RangeQueryResult call() {
        return dao.getElementsFromRange(this.startKey,this.endKey);
    }

}
