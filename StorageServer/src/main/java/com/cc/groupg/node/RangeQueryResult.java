package com.cc.groupg.node;

import com.cc.groupg.dao.Item;

import java.util.ArrayList;
import java.util.List;

public class RangeQueryResult {

    private List<Item> items;

    private BucketInformation bucketInformation;

    public RangeQueryResult() {
        items = new ArrayList<>();
        bucketInformation = new BucketInformation();
    }

    public RangeQueryResult(List<Item> items, BucketInformation bucketInformation) {
        this.items = items;
        this.bucketInformation = bucketInformation;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public BucketInformation getBucketInformation() {
        return bucketInformation;
    }

    public void setBucketInformation(BucketInformation bucketInformation) {
        this.bucketInformation = bucketInformation;
    }

    public void merge(RangeQueryResult other) {
        items.addAll(other.items);
        bucketInformation.merge(other.bucketInformation);
    }
}
