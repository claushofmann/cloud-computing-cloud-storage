package com.cc.groupg.node;

import com.cc.groupg.dao.Item;

public class SearchResult {

    private Item result;

    private BucketInformation bucketInformation;

    public SearchResult(Item result, BucketInformation bucketInformation) {
        this.result = result;
        this.bucketInformation = bucketInformation;
    }

    public Item getResult() {
        return result;
    }

    public void setResult(Item result) {
        this.result = result;
    }

    public BucketInformation getBucketInformation() {
        return bucketInformation;
    }

    public void setBucketInformation(BucketInformation bucketInformation) {
        this.bucketInformation = bucketInformation;
    }
}
