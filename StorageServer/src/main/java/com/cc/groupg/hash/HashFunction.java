package com.cc.groupg.hash;

public class HashFunction {

    int amountOfNodes;
    String[] hashValues;
    int hashStringLength;
    int keyDictSize;
    int lettersInNode;
    public static final String alphabet = "abcdefghijklmnopqrstuvwxyz";

    public HashFunction(int amountOfNodes) {
        this.amountOfNodes = amountOfNodes;
        hashValues = new String[amountOfNodes];
        hashStringLength = (int)(Math.log(amountOfNodes-1)/Math.log(alphabet.length()))+1;
        keyDictSize = (int)Math.pow(alphabet.length(),this.hashStringLength);
        lettersInNode = (int)Math.ceil((double)keyDictSize/amountOfNodes);

        for (int i =0;i<amountOfNodes;i++) {
            String hash = "";
            int toProcess = lettersInNode*i;
            for (int j = 0;j<hashStringLength;j++){
                hash = alphabet.charAt(toProcess%alphabet.length()) + hash;
                toProcess = toProcess/alphabet.length();
            }
            hashValues[i] = hash;
        }
    }
    public String getHash(String key) {
        if(key.length()<hashStringLength)
            for (int i=0;i<hashStringLength-key.length();i++) {
                key+='a';
            }
        String toCompute = key.toLowerCase().substring(0,hashStringLength);

        int positionInDict = 0;
        for (int i=0;i<hashStringLength;i++) {
            char letter = toCompute.charAt(i);
            positionInDict *= alphabet.length();
            positionInDict += alphabet.indexOf(letter);
        }
        int index = positionInDict/lettersInNode;
        return hashValues[index];
    }
    public String getStartHash(int nodePosition) {
        return this.hashValues[nodePosition];
    }

    public int getPositionByHash(String hash) {
        for (int i=0;i<hashValues.length;i++)
            if (hashValues[i].equals(hash))
                return i;
        return -1;
    }
    public int getAmountOfNodes() {
        return amountOfNodes;
    }
    /*
    public static void main(String[] args) {
        int amount = 10;
        HashFunction hash = new HashFunction(amount);
        System.out.println(hash.getHash("z"));
    }*/
}
