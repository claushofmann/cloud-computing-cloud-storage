package com.cc.groupg.rest.messages;

import java.util.ArrayList;
import java.util.List;

public class RangeQueryResponse {

    private List<RestKeyValuePair> records = new ArrayList<>();

    private StorageInfo storageInfo;

    public RangeQueryResponse() {
    }

    public RangeQueryResponse(List<RestKeyValuePair> records, StorageInfo storageInfo) {
        this.records = records;
        this.storageInfo = storageInfo;
    }

    public List<RestKeyValuePair> getRecords() {
        return records;
    }

    public void setRecords(List<RestKeyValuePair> records) {
        this.records = records;
    }

    public StorageInfo getStorageInfo() {
        return storageInfo;
    }

    public void setStorageInfo(StorageInfo storageInfo) {
        this.storageInfo = storageInfo;
    }
}
