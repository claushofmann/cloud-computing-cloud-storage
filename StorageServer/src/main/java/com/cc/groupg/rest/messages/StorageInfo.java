package com.cc.groupg.rest.messages;

import java.util.ArrayList;
import java.util.List;

public class StorageInfo {

    private List<String> fileNames;

    private List<Integer> nodeIds;

    public StorageInfo() {
    }

    public StorageInfo(List<String> fileNames, List<Integer> nodeIds) {
        this.fileNames = fileNames;
        this.nodeIds = nodeIds;
    }

    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public List<Integer> getNodeIds() {
        return nodeIds;
    }

    public void setNodeIds(List<Integer> nodeIds) {
        this.nodeIds = nodeIds;
    }
}
