package com.cc.groupg.rest.messages;

public class SearchResponse {

    private RestKeyValuePair record;

    private StorageInfo storageInfo;

    public RestKeyValuePair getRecord() {
        return record;
    }

    public void setRecord(RestKeyValuePair record) {
        this.record = record;
    }

    public StorageInfo getStorageInfo() {
        return storageInfo;
    }

    public void setStorageInfo(StorageInfo storageInfo) {
        this.storageInfo = storageInfo;
    }
}
