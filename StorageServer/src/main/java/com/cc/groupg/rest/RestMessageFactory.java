package com.cc.groupg.rest;

import com.cc.groupg.dao.Item;
import com.cc.groupg.node.BucketInformation;
import com.cc.groupg.node.EditResult;
import com.cc.groupg.node.RangeQueryResult;
import com.cc.groupg.node.SearchResult;
import com.cc.groupg.rest.messages.*;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class RestMessageFactory {

    private StorageInfo createStorageInfo(BucketInformation bucketInformation) {
        StorageInfo info = new StorageInfo();
        info.setFileNames(bucketInformation.getFilesUsed());
        info.setNodeIds(bucketInformation.getNodeIdsUsed());
        return info;
    }

    private RestKeyValuePair createRestKeyValuePair(Item item) {
        if (item != null) {
            RestKeyValuePair keyValuePair = new RestKeyValuePair();
            keyValuePair.setKey(item.getKey());
            keyValuePair.setValue(item.getValue());
            return keyValuePair;
        } else {
            return null;
        }
    }

    public InsertResponse createInsertResponse(EditResult result) {
        InsertResponse response =  new InsertResponse();
        response.setInserted(result.getSuccess());
        response.setStorageInfo(createStorageInfo(result.getBucketInformation()));
        return response;
    }

    public DeleteResponse createDeleteResponse(EditResult result) {
        DeleteResponse response = new DeleteResponse();
        response.setDeleted(result.getSuccess());
        response.setStorageInfo(createStorageInfo(result.getBucketInformation()));
        return response;
    }

    public SearchResponse createSearchResponse(SearchResult result) {
        SearchResponse response = new SearchResponse();
        response.setRecord(createRestKeyValuePair(result.getResult()));
        response.setStorageInfo(createStorageInfo(result.getBucketInformation()));
        return response;
    }

    public RangeQueryResponse createRangeQueryResponse(RangeQueryResult result) {
        RangeQueryResponse response = new RangeQueryResponse();
        response.setRecords(result.getItems()
                .stream()
                .map(this::createRestKeyValuePair)
                .collect(Collectors.toList()));
        response.setStorageInfo(createStorageInfo(result.getBucketInformation()));
        return response;
    }

}
