package com.cc.groupg.rest;

import com.cc.groupg.master.MasterNode;
import com.cc.groupg.node.EditResult;
import com.cc.groupg.node.RangeQueryResult;
import com.cc.groupg.node.SearchResult;
import com.cc.groupg.rest.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class RestApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestApi.class);

    private MasterNode storageMasterNode = new MasterNode(13);

    @Autowired
    private RestMessageFactory messageFactory;

    @PostMapping("/data/{key}")
    public ResponseEntity<InsertResponse> insert(@PathVariable String key, @RequestBody String value) {
        LOGGER.info("Received value insertion for " + key + ": " + value);
        EditResult result = storageMasterNode.insert(key, value);
        InsertResponse response = messageFactory.createInsertResponse(result);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("data/{key}")
    public ResponseEntity<DeleteResponse> delete(@PathVariable String key) {
        LOGGER.info("Received deletion request for key " + key);
        EditResult result = storageMasterNode.delete(key);
        DeleteResponse response = messageFactory.createDeleteResponse(result);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("data/{key}")
    public ResponseEntity<SearchResponse> search(@PathVariable String key) {
        LOGGER.info("Received search request for key " + key);
        SearchResult result = storageMasterNode.search(key);
        SearchResponse response = messageFactory.createSearchResponse(result);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Can be accessed like this: localhost:8080/rangeQuery?start={keyStart}&end={keyEnd}
    @GetMapping("/rangeQuery")
    public ResponseEntity<RangeQueryResponse> rangeQuery(@RequestParam("start") String keyStart, @RequestParam("end") String keyEnd) {
        LOGGER.info("Received range query between keys " + keyStart + " and " + keyEnd);
        RangeQueryResult result = storageMasterNode.rangeQuery(keyStart, keyEnd);
        RangeQueryResponse response = messageFactory.createRangeQueryResponse(result);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
