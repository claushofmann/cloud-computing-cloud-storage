package com.cc.groupg.master;

import com.cc.groupg.dao.Item;
import com.cc.groupg.hash.HashFunction;
import com.cc.groupg.node.*;

import java.util.HashMap;
import java.util.Map;


public class MasterNode {

    private Map<String, Node> storageNodes = new HashMap<>();

    private HashFunction hashFunction;

    public MasterNode(int numberStorageNodes) {
        hashFunction = new HashFunction(numberStorageNodes);
        Node nextNode = null;
        for (int i = numberStorageNodes - 1; i >= 0; --i) {
            String startHash = hashFunction.getStartHash(i);
            Node node = new Node(startHash, "storage_bucket_" + i, hashFunction, i);
            storageNodes.put(startHash, node);
            if (nextNode != null) {
                node.setNextNode(nextNode);
            }
            if(i==0) {
                Node firstNode = storageNodes.get(hashFunction.getStartHash(numberStorageNodes-1));
                firstNode.setNextNode(node);
                storageNodes.get(hashFunction.getStartHash(numberStorageNodes-2)).setNextNode(firstNode);
            }
            nextNode = node;
        }
    }

    private Node getNodeForKey(String key) {
        String nodeHash = hashFunction.getHash(key);
        return storageNodes.get(nodeHash);
    }

    public EditResult insert(String key, String value) {
        Node insertNode = getNodeForKey(key);
        if(insertNode.getStatus() == 1) {
            return insertNode.insert(new Item(key, value),false);
        } else {
            int nextIndex = hashFunction.getPositionByHash(hashFunction.getHash(key))+1;
            Node node = storageNodes.get(hashFunction.getStartHash(nextIndex));
            if(node.getStatus() == 1)
                return node.insert(new Item(key, value),true);
            else
                return new EditResult(false, new BucketInformation());
        }

    }

    public EditResult delete(String key) {
        Node deleteNode = getNodeForKey(key);
        if(deleteNode.getStatus() == 1) {
            return deleteNode.delete(key, false);
        } else {
            int nextIndex = hashFunction.getPositionByHash(hashFunction.getHash(key))+1;
            Node node = storageNodes.get(hashFunction.getStartHash(nextIndex));
            if(node.getStatus() == 1)
                return node.delete(key,true);
            else
                return new EditResult(false, new BucketInformation());
        }
    }

    public SearchResult search(String key) {
        Node searchNode = getNodeForKey(key);
        if(searchNode.getStatus() == 1) {
            return searchNode.search(key, false);
        } else {
            int nextIndex = hashFunction.getPositionByHash(hashFunction.getHash(key))+1;
            Node node = storageNodes.get(hashFunction.getStartHash(nextIndex));
            if(node.getStatus() == 1)
                return node.search(key,true);
            else
                return new SearchResult(null,new BucketInformation());
        }
    }

    public RangeQueryResult rangeQuery(String startKey, String endKey) {
        Node startNode = getNodeForKey(startKey);
        if(startNode.getStatus() == 1) {
            return startNode.searchInRange(startKey, endKey, false);
        } else {
            int nextIndex = hashFunction.getPositionByHash(hashFunction.getHash(startKey))+1;
            Node node = storageNodes.get(hashFunction.getStartHash(nextIndex));
            if(node.getStatus() == 1)
                return node.searchInRange(startKey, endKey,true);
            else return new RangeQueryResult();

        }
    }

}
