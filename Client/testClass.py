from urllib import request, parse
import csv
from datetime import datetime
import logging
import random
import json

data = [];
baseUrl = "http://localhost:8080/";
testDataName = "testData.csv";
numOfProcesses = 3  ; # data from at least numOfProcesses processes
numOfRandomlyDeletedEntries = 2;

def executeRangeQuery():
    headers = {'Accept': 'application/json'};
    req =  request.Request(baseUrl + "/rangeQuery?start=a&end=z", headers=headers, method='GET');
    try:
        resp = request.urlopen(req);
        jsonResponse = json.loads(resp.read());
        logging.info("Range Quary completed");
        logging.info("response: " + str(jsonResponse));
    except urllib.error.HTTPError as e:
        logging.info(e)
        
def selectRandomlyDataFromDatabase(nameOfCsv):
    usedProcesses = [];
    while True:
        randomValue = random.randrange(0, len(data) - 1, 1);
        toSelect = data[randomValue]['key'];
        headers = {'Accept': 'application/json'};
        req =  request.Request(baseUrl + "/data/" + toSelect, headers=headers, method='GET');
        try:
            resp = request.urlopen(req);
            jsonResponse = json.loads(resp.read());
            for nodeId in jsonResponse['storageInfo']['nodeIds']:
                if nodeId not in usedProcesses:
                    usedProcesses.append(nodeId);
            logging.info("select(" + data[randomValue]['key'] + "," + data[randomValue]['value'] + ") completed");
            logging.info("response: " + str(jsonResponse));
            if (len(usedProcesses) == numOfProcesses):
                break;
        except urllib.error.HTTPError as e:
            logging.info("select(" + data[randomValue]['key'] + "," + data[randomValue]['value'] + ") error: " + e.code);
    

def deleteRandomlyFromDatabase(nameOfCsv):
    for i in range (0, numOfRandomlyDeletedEntries):
        randomValue = random.randrange(0, len(data) - 1, 1);
        toDelete = data[randomValue]['key'];
        headers = {'Accept': 'application/json'};
        req =  request.Request(baseUrl + "/data/" + toDelete, headers=headers, method='DELETE');
        try:
            resp = request.urlopen(req);
            jsonResponse = json.loads(resp.read());
            logging.info("delete(" + data[randomValue]['key'] + "," + data[randomValue]['value'] + ") completed");
            logging.info("response: " + str(jsonResponse));
        except urllib.error.HTTPError as e:
            logging.info("delete(" + data[randomValue]['key'] + "," + data[randomValue]['value'] + ") error: " + e.code);
        

def insertIntoDatabase(nameOfCsv):
    for idx,row in enumerate(data):
        value = row['value'];
        value = value.encode('utf-8')
        headers = {'content-type': 'text/plain', 'Accept': 'application/json'};
        req =  request.Request(baseUrl + "/data/" + row['key'], data=value, headers=headers, method='POST');
        try:
            resp = request.urlopen(req);
            jsonResponse = json.loads(resp.read());
            logging.info("insert(" + row['key'] + "," + row['value'] + ") completed");
            logging.info("response: " + str(jsonResponse));
        except urllib.error.HTTPError as e:
            logging.info("insert(" + row['key'] + "," + row['value'] + ") error: " + e.code);

def readFromCsv(nameOfCsv):
    with open(nameOfCsv, newline = '') as csvfile:
        reader = csv.DictReader(csvfile);
        for idx,row in enumerate(reader): 
            data.append(row);


if "__main__":
    logging.basicConfig(filename = 'logs.log', level = logging.DEBUG, format = '%(asctime)s %(message)s');

    # Reading from File
    print("Starting reading data from csv file " + testDataName);
    logging.info("Starting reading data from csv file " + testDataName);
    readFromCsv(testDataName);
    logging.info("Reading is done");
    print("Reading is done");

    # Inserting to the Database.
    print("Inserting to the database");
    logging.info("Inserting to the database");
    insertIntoDatabase(testDataName);
    logging.info("Insertion is done");
    print("Insertion is done");

    # Random selection
    print("Starting random selection for " + str(numOfProcesses) + " processes");
    logging.info("Starting random selection for " + str(numOfProcesses) + " processes");
    selectRandomlyDataFromDatabase(testDataName);
    logging.info("Selection is done");
    print("Selection is done");

    # Random deleting.
    print("Delete " + str(numOfRandomlyDeletedEntries) + " random values from Database");
    logging.info("Delete " + str(numOfRandomlyDeletedEntries) + " random values from Database");
    deleteRandomlyFromDatabase(testDataName);
    logging.info("Deletion is done");
    print("Deletion is done");

    # Range query.
    print("Execution of range query");
    logging.info("Execution of range query");
    executeRangeQuery();
    logging.info("Range query execution is done");
    print("Range query execution is done");

    print("See the results in logs.log");
